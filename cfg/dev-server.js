const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');

const dir = require('./dir');
const config = require('./webpack-dev.js');

const port = 3000;
const host = 'localhost';

const options = {
  host,
  hot: true,
  overlay: {
    warnings: false,
    errors: true,
  },
  quiet: false,
  noInfo: false,
  contentBase: dir.assets,
  watchContentBase: true,
  after() {
    process.stdout.write(`Dev server is running: http://${host}:${port}\n`);
  }
};

WebpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config);
const server = new WebpackDevServer(compiler, options);

server.listen(port, host, () => {});
