import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './FlowToggle.scss';

const FLOW_MODE_INCOME = 'income';
const FLOW_MODE_OUTCOME = 'outcome';
const FLOW_MODE = [FLOW_MODE_INCOME, FLOW_MODE_OUTCOME];
Object.freeze(FLOW_MODE);

const FlowToggle = ({ mode, handleClick }) => {
  const isIncome = mode === FLOW_MODE_INCOME;

  return (
    <div className={styles.root}>
      <div
        className={classNames(styles.toggle, {
          [styles.plus]: isIncome,
          [styles.plusHoverable]: !isIncome,
        })}
        onClick={isIncome ? null : handleClick}
      >
        +
      </div>
      <div
        className={classNames(styles.toggle, {
          [styles.minus]: !isIncome,
          [styles.minusHoverable]: isIncome,
        })}
        onClick={isIncome ? handleClick : null}
      >
        −
      </div>
    </div>
  );
};

FlowToggle.propTypes = {
  mode: PropTypes.oneOf(FLOW_MODE).isRequired,
  handleClick: PropTypes.func.isRequired,
};

export { FLOW_MODE, FLOW_MODE_INCOME, FLOW_MODE_OUTCOME, FlowToggle };
