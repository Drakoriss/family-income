import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Category.scss';

const Category = ({ className, list }) => (
  <select className={classNames(styles.container, className)}>
    {list.map(o => (
      <option value={o.id} key={o.id}>
        {o.value}
      </option>
    ))}
  </select>
);

Category.propTypes = {
  className: PropTypes.string,
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
};

Category.defaultProps = {
  className: null,
};

export { Category };
