import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Input.scss';

const financeRE = /[\d.]+/;

const handleBeforeInput = e => {
  const input = e.data === ',' ? '.' : e.data;

  if (input === '.' && e.target.value.includes('.')) {
    e.preventDefault();
    return;
  }

  if (!financeRE.test(input)) {
    e.preventDefault();
  }
};

const handleKeyPress = e => {
  if (e.key === ',') {
    const cursorAt = e.target.selectionStart;
    const cursorTo = e.target.selectionEnd;
    e.target.value = e.target.value.replace(',', '.');
    e.target.selectionStart = cursorAt;
    e.target.selectionEnd = cursorTo;
  }
};

const Input = ({ className, maxLength }) => (
  <input
    className={classNames(styles.input, className)}
    maxLength={maxLength}
    onBeforeInput={handleBeforeInput}
    onKeyUp={handleKeyPress}
  />
);

Input.propTypes = {
  className: PropTypes.string,
  maxLength: PropTypes.number,
};

Input.defaultProps = {
  className: null,
  maxLength: 16,
};

export { Input };
