import { createAction, defaultHandler } from '../helpers';

const ADD_FLOW = 'FLOW/ADD';
const addFlow = createAction(ADD_FLOW);

const flowHandlers = {
  [ADD_FLOW]: defaultHandler,
};

export { addFlow, flowHandlers };
