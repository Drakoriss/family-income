import { toImmutable } from '../../utils/object';
import { flowHandlers } from './actions';

const initialState = toImmutable({});

function flowReducer(state = initialState, action) {
  if (flowHandlers[action.type]) {
    return flowHandlers[action.type](state, action.payload);
  }

  return state;
}

export { flowReducer };
