import { combineReducers } from 'redux';
import { flowReducer } from './flow';

const rootReducer = combineReducers({
  flow: flowReducer,
});

export { rootReducer };
