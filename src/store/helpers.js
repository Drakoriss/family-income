function createAction(type) {
  return payload => ({ type, payload });
}

function defaultHandler(state, payload) {
  return { ...state, ...payload };
}

export { createAction, defaultHandler };
