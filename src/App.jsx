import React from 'react';
import { hot } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { Month } from './components/month';
import { rootReducer } from './store';
import styles from './App.scss';

const store = createStore(
  rootReducer,
  applyMiddleware(createLogger({ collapsed: true, diff: true })),
);

const App = () => (
  <Provider store={store}>
    <div className={styles.root}>
      <Month />
    </div>
  </Provider>
);

export default hot(module)(App);
