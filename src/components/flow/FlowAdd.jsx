import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Flow.scss';
import {
  Category,
  FlowToggle,
  Input,
  FLOW_MODE,
  FLOW_MODE_INCOME,
  FLOW_MODE_OUTCOME,
} from '../../controls';

const list = [
  { id: 'one', value: 'раз' },
  { id: 'two', value: 'два' },
  { id: 'three', value: 'три' },
  { id: 'four', value: 'ще якась фігня чо' },
];

function FlowAdd({ initialMode }) {
  const [mode, setMode] = useState(initialMode);

  return (
    <>
      <div className={styles.content}>
        <FlowToggle
          mode={mode}
          handleClick={() =>
            setMode(
              mode === FLOW_MODE_INCOME ? FLOW_MODE_OUTCOME : FLOW_MODE_INCOME,
            )
          }
        />
        <Input />
        <Category list={list} />
      </div>
      <div className={styles.footer}>
        <button type="button" className={styles.button}>
          OK
        </button>
        <button type="button" className={styles.button}>
          Cancel
        </button>
      </div>
    </>
  );
}

FlowAdd.propTypes = {
  initialMode: PropTypes.oneOf(FLOW_MODE).isRequired,
};

export { FlowAdd };
