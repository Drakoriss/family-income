import React from 'react';
import PropTypes from 'prop-types';
import styles from './Flow.scss';
import { FLOW_MODE_INCOME, FLOW_MODE_OUTCOME } from '../../controls/flowToggle';

const FlowInfo = ({ setMode }) => (
  <>
    <div className={styles.content}>
      <span className={styles.incomeText}>543.02</span>
      <span className={styles.rawText}>-543.02</span>
      <span className={styles.outcomeText}>0.00</span>
    </div>
    <div className={styles.footer}>
      <div>
        <button type="button" onClick={() => setMode(FLOW_MODE_INCOME)}>
          <Icon className={styles.incomeIcon} />
        </button>
        <button type="button" onClick={() => setMode(FLOW_MODE_OUTCOME)}>
          <Icon className={styles.outcomeIcon} />
        </button>
      </div>
      <button type="button">
        <Icon className={styles.detailsIcon} />
      </button>
    </div>
  </>
);

FlowInfo.propTypes = {
  setMode: PropTypes.func.isRequired,
};

const Icon = ({ className }) => <div className={className} />;

Icon.propTypes = {
  className: PropTypes.string.isRequired,
};

export { FlowInfo };
