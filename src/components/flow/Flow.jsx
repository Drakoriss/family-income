import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Flow.scss';
import { FlowAdd } from './FlowAdd';
import { FlowInfo } from './FlowInfo';

function Flow({ day, className }) {
  const [mode, setMode] = useState(null);

  return (
    <div className={classNames(styles.root, className)}>
      <span className={styles.header}>{day}</span>
      {mode ? <FlowAdd initialMode={mode} /> : <FlowInfo setMode={setMode} />}
    </div>
  );
}

Flow.propTypes = {
  day: PropTypes.number.isRequired,
  className: PropTypes.string,
};

Flow.defaultProps = {
  className: null,
};

export { Flow };
