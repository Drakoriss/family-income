import React from 'react';
import classNames from 'classnames';
import { Flow } from '../flow';
import { DaysOfTheWeek } from './DaysOfTheWeek';
import styles from './Month.scss';
import { daysInMonth } from '../../utils/date';

const prefill = () => {
  const date = new Date();
  date.setDate(1);
  const weekDay = date.getDay();
  const quantity = weekDay > 0 ? weekDay - 1 : 6;

  return Array.from({ length: quantity }).map((_, i) => (
    <div className={styles.emptyItem} key={`fake#${i}`} />
  ));
};

const fill = (quantity, current) =>
  Array.from({ length: quantity }).map((_, i) => (
    <Flow
      className={classNames(styles.flowItem, {
        // eslint-disable-next-line
        [styles.current]: ++i === current,
      })}
      day={i}
      key={`real#${i}`}
    />
  ));

const Month = () => {
  const date = new Date();
  const weekDay = date.getDay();
  const currentDay = weekDay > 0 ? weekDay - 1 : 6;
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();

  return (
    <div>
      <DaysOfTheWeek
        current={currentDay}
        listClassName={styles.list}
        dayClassName={styles.item}
      />
      <div className={styles.root}>
        {prefill()}
        {fill(daysInMonth(month, year), day)}
      </div>
    </div>
  );
};

export { Month };
