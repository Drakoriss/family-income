import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './DaysOfTheWeek.scss';

const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

const DaysOfTheWeek = props => {
  const { current, listClassName, dayClassName } = props;

  return (
    <div className={classNames(styles.root, listClassName)}>
      {days.map((d, i) => (
        <span
          className={classNames(styles.day, dayClassName, {
            [styles.current]: i === current,
          })}
          key={d}
        >
          {d}
        </span>
      ))}
    </div>
  );
};

DaysOfTheWeek.propTypes = {
  current: PropTypes.number.isRequired,
  listClassName: PropTypes.string,
  dayClassName: PropTypes.string,
};

DaysOfTheWeek.defaultProps = {
  listClassName: null,
  dayClassName: null,
};

export { DaysOfTheWeek };
