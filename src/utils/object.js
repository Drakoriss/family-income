function toImmutable(obj) {
  Object.values(obj).forEach(o => typeof o === 'object' && toImmutable(o));
  Object.freeze(obj);
  return obj;
}

export { toImmutable };
